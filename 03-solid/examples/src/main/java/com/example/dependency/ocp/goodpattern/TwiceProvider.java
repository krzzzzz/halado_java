package com.example.dependency.ocp.goodpattern;

public class TwiceProvider implements ValueProvider {
    public int get(int value) {
        return value *= 2;
    }
}
