package com.example.dependency.ocp.goodpattern;

public class Minus2Response implements ValueProvider {
    public int get(int value) {
        return -2;
    }
}
