package com.example.dependency.Liskov.lspviolation;

import java.util.List;

public class Demo {

    public static void main(String args[]) {
        List<Integer> data = (new Measurement()).getData();
        Experiment.run(new Analize(), data);
    }
}
