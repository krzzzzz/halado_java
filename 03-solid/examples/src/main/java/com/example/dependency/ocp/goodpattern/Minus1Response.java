package com.example.dependency.ocp.goodpattern;

public class Minus1Response implements ValueProvider {
    public int get(int value) {
        return -1;
    }
}
