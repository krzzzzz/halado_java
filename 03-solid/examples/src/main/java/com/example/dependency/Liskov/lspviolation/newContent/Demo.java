package com.example.dependency.Liskov.lspviolation.newContent;

import com.example.dependency.Liskov.lspviolation.Experiment;
import com.example.dependency.Liskov.lspviolation.Measurement;

import java.util.List;

public class Demo {

    public static void main(String args[]) {
        List<Integer> data = (new Measurement()).getData();
        Experiment.run(new LaterAnalize(), data);
    }
}
