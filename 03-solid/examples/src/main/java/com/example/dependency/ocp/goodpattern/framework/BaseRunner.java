package com.example.dependency.ocp.goodpattern.framework;

import com.example.dependency.ocp.goodpattern.framework.user.PartiallyImplementFlow;

public class BaseRunner {
    public static void main(String args[]) {
        run(new PartiallyImplementFlow());
    }

    private static void run(BaseFlow baseFlow) {
        baseFlow.run();
    }
}
