package com.example.dependency.ocp.goodpattern.school;

public class Teacher extends User {
    public String getDashboard() {
        return "courseSubscriber, takeExam" + super.getDashboard();
    }
}
