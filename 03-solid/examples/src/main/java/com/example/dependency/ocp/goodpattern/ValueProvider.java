package com.example.dependency.ocp.goodpattern;

public interface ValueProvider {
    public int get(int value);
}
