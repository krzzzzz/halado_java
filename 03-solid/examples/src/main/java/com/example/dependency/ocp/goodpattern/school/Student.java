package com.example.dependency.ocp.goodpattern.school;

public class Student extends User{
    public String getDashboard() {
        return "coursePublisher, createExam, " + super.getDashboard();
    }
}
