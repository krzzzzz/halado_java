package com.example.dependency.ocp.goodpattern;

public interface ResponseFactory {
    ValueProvider get(int value);
}
