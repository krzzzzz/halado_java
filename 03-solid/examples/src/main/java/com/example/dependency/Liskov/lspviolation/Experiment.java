package com.example.dependency.Liskov.lspviolation;

import java.util.List;

public class Experiment {

    public static void run(Analize analize, List<Integer> data) {
        System.out.println(analize.process(data));
    }

}
