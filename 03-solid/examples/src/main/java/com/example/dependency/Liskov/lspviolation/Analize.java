package com.example.dependency.Liskov.lspviolation;

import java.security.InvalidParameterException;
import java.util.List;

public class Analize {
    /**
     * important behaviour
     *
     * preconditions:
     *  - integers int the array (any). No restriction against parameter values.
     *
     *  post condition:
     *   - average (in return value)
     *
     * @param data measured values
     * @return the average of the values
     */
    public Double process(List<Integer> data) {
        assertNotNullOrNotEmptyData(data);

        Double sum = 0.0;

        for (Integer item: data) {
            sum += item;
        }

        return sum / data.size();
    }

    protected void assertNotNullOrNotEmptyData(List<Integer> data) {
        if (data == null || data.isEmpty()) {
            throw new InvalidParameterException("measurements data cannot be null");
        }
    }
}
