package com.example.dependency;

import com.example.dependency.ocp.BadPattern.BigQuestion;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependencyApplication {

    public static void main(String[] args) {

        System.out.println(new BigQuestion().get(42));
        System.out.println(new BigQuestion().get(43));
        System.out.println(new BigQuestion().get(34));
        System.out.println(new BigQuestion().get(-10));
        System.out.println(new BigQuestion().get(100));
        System.out.println(new BigQuestion().get(-51));

    }

}
