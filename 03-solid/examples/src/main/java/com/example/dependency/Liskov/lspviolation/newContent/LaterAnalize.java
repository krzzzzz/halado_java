package com.example.dependency.Liskov.lspviolation.newContent;

import com.example.dependency.Liskov.lspviolation.Analize;

import java.security.InvalidParameterException;
import java.util.List;

public class LaterAnalize extends Analize {

    /**
     * important behaviour
     *
     * preconditions:
     *  - integers int the array (just positive).
     *     Strong restriction against negative values.
     *
     *  post condition:
     *   - sum (in return value)
     *
     * @param data measured values
     * @return the sum of the values
     */
    public Double process(List<Integer> data) {
        assertNotNullOrNotEmptyData(data);
        assertNotZeroData(data);
        return getSum(data);
    }

    private Double getSum(List<Integer> data) {
        Double sum = 0.0;

        for (Integer item: data) {
            sum += item;
        }
        return sum;
    }

    protected void assertNotZeroData(List<Integer> data) {
        for (Integer item: data) {
            if (item < 0) {
                throw new InvalidParameterException("measurements data cannot be negative");
            }
        }
    }
}
