package haladojava.registration;

import haladojava.registration.validator.LengthValidator;
import haladojava.registration.validator.SpaceValidator;
import haladojava.registration.validator.Validator;

import java.util.ArrayList;
import java.util.List;

public class Komplex {

    public static void main(String[] args) {
        UseRepository useRepository = new UseRepository();
        List<Validator> validators = new ArrayList<>();
        validators.add(new LengthValidator());
        validators.add(new SpaceValidator());
        UserService userService = new UserServiceImpl(
                useRepository, validators
        );

        UserController userController = new UserController(
                userService, new NeptunCodeGenerator()
        );


        userController.save(new UserDto(" thereIsSpace"));
        userController.save(new UserDto("short"));
        userController.save(new UserDto("cool_example"));
    }
}
