package haladojava.registration;

public class User {
    private final String name;
    private final boolean enabled;
    private final String neptunkod;

    public User(String name, boolean enabled, String neptunkod) {
        this.name = name;
        this.enabled = enabled;
        this.neptunkod = neptunkod;
    }

    public String getName() {
        return name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getNeptunkod() {
        return neptunkod;
    }
}
