package haladojava.registration;

public class UserController {
    private final UserService userService;
    private final NeptunCodeGenerator neptunCodeGenerator;

    public UserController(UserService userService, NeptunCodeGenerator neptunCodeGenerator) {
        this.userService = userService;
        this.neptunCodeGenerator = neptunCodeGenerator;
    }

    public void save(UserDto userDto) {
        userService.save(new User(userDto.getName(), true,
                String.format("neptun %d", neptunCodeGenerator.getNextCounter())));
    }

}
