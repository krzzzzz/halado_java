package haladojava.registration.validator;

import haladojava.registration.User;

public class SpaceValidator implements Validator {
    @Override
    public boolean isValid(User user) {
        return user.getName().indexOf(" ") == -1;
    }
}
