package haladojava.registration.validator;

import haladojava.registration.User;

public class LengthValidator implements Validator {
    public final int MAX_LENGTH = 6;

    @Override
    public boolean isValid(User user) {
        return user.getName().length() > MAX_LENGTH;
    }
}
