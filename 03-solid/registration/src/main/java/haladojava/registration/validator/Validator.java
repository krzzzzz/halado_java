package haladojava.registration.validator;

import haladojava.registration.User;

public interface Validator {
    boolean isValid(User user);
}
