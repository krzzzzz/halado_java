package com.example.dependency.general.strongDependency.composition;

import com.example.dependency.general.strongDependency.Worker;

/**
 * Demo class for Strong dependency. Bad pattern.
 * The class store reference to class and creates it.
 * The dependency can be set once for every instance when creating.
 *
 * usage:
 *   CompositionDepInConstructor compositionDepInConstructor =
 *                 new CompositionDepInConstructor(new Worker());
 *
 *   // the Worker.test is used
 *   compositionDepInConstructor.feature();
 *
 *   CompositionDepInConstructor compositionDepInConstructor2 =
 *                 new CompositionDepInConstructor(new RefinedWorker());
 *
 *   // the Refined.test is used
 *   compositionDepInConstructor2.feature();
 *
 * Flexibility: **
 * Mandatory: YES
 * Testability: **
 */
public class CompositionDepInConstructor {
    /**
     * Strong dependency
     * The reference can store Worker class or any inherited class instance only.
     * The reference can be set before usage f.e. constructor
     */
    private final Worker worker;

    /**
     * Not default constructor
     * The dependency is visible from outside
     * @param worker Worker or a special worker
     */
    public CompositionDepInConstructor(Worker worker) {
        this.worker = worker;
    }

    /**
     * Can be mocked in the test.
     * When this is called then the worker.test is called as well.
     * The test method implementation can be changed before usage.
     */
    public void feature() {
        worker.test();
    }
}
