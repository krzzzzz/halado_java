package com.example.dependency.general.strongDependency.composition;

import com.example.dependency.general.strongDependency.Worker;

/**
 * Demo class for Strong dependency. Bad pattern.
 * The class store reference to class and creates it.
 * Hidden dependency
 *
 *  Usage:
 *   Composition composition = new Composition();
 *   composition.feature();
 *
 * Flexibility: *
 * Mandatory: YES
 * Testability: *
 */
public class Composition {
    /**
     * Strong dependency
     * The reference can store Worker class instance only.
     * The dependency is not visible from outside
     */
    private Worker worker = new Worker();

    /**
     * No chance to mock during test.
     * When this is called then the worker.test is called as well.
     * The test method implementation can be changed at runtime.
     */
    public void feature() {
        worker.test();
    }
}
