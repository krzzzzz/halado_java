package com.example.dependency.general.weakDependency;

import com.example.dependency.general.weakDependency.Worker;

public class HardWorker implements Worker {
    @Override
    public void test() {
        System.out.println("HardWorker");
    }
}
