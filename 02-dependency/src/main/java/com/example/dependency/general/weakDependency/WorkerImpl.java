package com.example.dependency.general.weakDependency;

import com.example.dependency.general.weakDependency.Worker;

public class WorkerImpl implements Worker {
    @Override
    public void test() {
        System.out.println("Worker");
    }
}
