package com.example.dependency.general.impl;

import com.example.dependency.general.weakDependency.WorkerImpl;

public class NewWorkerImpl extends WorkerImpl {

    @Override
    public void test() {
        System.out.println("NewWorker");
    }
}
