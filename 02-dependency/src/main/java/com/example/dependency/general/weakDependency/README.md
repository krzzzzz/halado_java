This is the suggested implementation of the depepndency
in case of any Dependency Injection 
f.e. Spring.

When the system want to create the managed bean
(*CompositionDepInConstructor* or *DepInParameter*)
it realizes the dependency.
The dependency instance will be created then the required instance.

Practice

Implementation of pets demo.

There are two kind of pets
 - cat
 - dog

Both of them implement the speak method.
- miau
- vau
