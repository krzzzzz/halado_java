package com.example.dependency.general.strongDependency;

/**
 * Special Worker (new version)
 */
public class RefinedWorker extends Worker {

    /**
     * new implementation of the behaviour
     */
    @Override
    public void test() {
        System.out.println("refined test implementation");
    }
}
