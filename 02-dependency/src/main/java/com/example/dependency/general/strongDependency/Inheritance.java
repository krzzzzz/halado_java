package com.example.dependency.general.strongDependency;

/**
 * Demo class for Strong dependency. Bad pattern.
 * This class is a special worker.
 * Replacing the behavior can be established by new derived class.
 * The accessibly of the inherited method depends on the base class definition
 *
 *  Usage:
 *    Inheritance inheritance = new Inheritance();
 *    inheritance.feature();
 *
 * Flexibility: *
 * Mandatory: YES
 * Testability: *
 */
public class Inheritance extends Worker {
    /**
     * No chance to mock during test.
     * When this is called then the inherited method is called as well.
     * The test method implementation can be changed at runtime.
     */
    public void feature() {
        test();
    }
}
