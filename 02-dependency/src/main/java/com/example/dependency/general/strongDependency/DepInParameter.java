package com.example.dependency.general.strongDependency;

import com.example.dependency.general.strongDependency.Worker;

/**
 * Demo class for Strong dependency. Bad pattern.
 * The class store reference to class and creates it.
 * Mandatory dependency
 *
 * Usage:
 *         DepInParameter depInParameter = new DepInParameter();
 *
 *         depInParameter.feature(new Worker());
 *
 *         depInParameter.feature(new RefinedWorker());
 *
 * Flexibility: ***
 * Mandatory: YES
 * Testability: **
 */
public class DepInParameter {

    /**
     * Can be mocked in the test.
     * When this is called then the worker.test is called as well (or test of refined worker).
     * The test method implementation can be changed before usage.
     * Strong dependency
     * Mandatory dependency

     * @param worker Worker or a special worker (or refined worker)
     */
    public void feature(Worker worker) {
        worker.test();
    }
}
