package com.example.dependency.general.strongDependency.composition;

import com.example.dependency.general.strongDependency.Worker;

/**
 * Demo class for Strong dependency. Bad pattern.
 * The class store reference to class and creates it.
 * Optional dependency
 *
 * usage:
 *   CompositionDepInSetter compositionDepInSetter = new CompositionDepInSetter();
 *
 *   // the dependency is not initialized, dependency is not used
 *   compositionDepInSetter.feature();
 *
 *   compositionDepInSetter.setWorker(new Worker());
 *
 *   // Worker.test is used
 *   compositionDepInSetter.feature();
 *
 *   compositionDepInSetter.setWorker(new RefinedWorker());
 *
 *   // same method call, but the RefinedWorker.test is used
 *   compositionDepInSetter.feature();
 *
 * Flexibility: ***
 * Mandatory: NO
 * Testability: **
 */
public class CompositionDepInSetter {
    /**
     * Strong dependency
     * The reference can store Worker class or any inherited class instance only.
     * The reference can be set at runtime.
     * Optional dependency
     */
    private Worker worker;

    /**
     *
     * @param worker Any Worker or refined worker can be stored
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * Can be mocked in the test.
     * When this is called then the worker.test is called as well.
     * The test method implementation can be changed before usage (anytime).
     */
    public void feature() {

        // The reference has to be tested because the setter will set it up
        // If no setter the null value in reference
        if (worker != null) {
            worker.test();
        }
    }
}
