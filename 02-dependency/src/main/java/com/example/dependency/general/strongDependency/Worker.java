package com.example.dependency.general.strongDependency;

/**
 * Demo class for a dependency
 */
public class Worker {
    public void test() {
        System.out.println("Worker");
    }
}
