package com.example.dependency.interfaceusage.cache;

public interface CacheRead {
    int get(String name);
    boolean empty();
}
