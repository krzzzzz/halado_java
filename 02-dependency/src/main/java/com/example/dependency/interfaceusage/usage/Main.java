package com.example.dependency.interfaceusage.usage;

import com.example.dependency.interfaceusage.cache.Cache;

public class Main {
    public static void main(String args[]) {

        Cache cache = new Cache();

        cache.set("titok", 123);

        Usage usage = new Usage(cache);
        usage.run();
    }
}
