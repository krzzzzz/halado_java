package com.example.dependency.interfaceusage.usage;

import com.example.dependency.interfaceusage.cache.CacheRead;

public class Usage {
    private final CacheRead cacheRead;
    public Usage(CacheRead cacheRead) {
        this.cacheRead = cacheRead;
    }

    public void run() {
        System.out.println(cacheRead.get("titok"));
    }
}
