# hexagon - onion - clean architecture

original article
https://alistair.cockburn.us/hexagonal-architecture/

## recipe 

### adapter/web/controller
- create command
- use the use case

### adapter/persistence
- implements output port
- use the repository

### application/service 
- implements the use case (input port)
- contains all the business rule validations
- implements the business logic 
- input from Command
- output to the output port (probably repository)

### application/port
- in: use case interfaces
  - use case class has no dependency
- out: db, external http endpoint usage interfaces