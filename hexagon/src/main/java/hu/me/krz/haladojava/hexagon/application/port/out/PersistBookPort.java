package hu.me.krz.haladojava.hexagon.application.port.out;

import hu.me.krz.haladojava.hexagon.domain.Book;

public interface PersistBookPort {
	Book saveBook(Book book);
}
