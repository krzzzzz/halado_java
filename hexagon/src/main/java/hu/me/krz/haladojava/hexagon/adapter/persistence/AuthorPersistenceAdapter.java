package hu.me.krz.haladojava.hexagon.adapter.persistence;

import java.util.Optional;

import org.springframework.stereotype.Component;

import hu.me.krz.haladojava.hexagon.application.port.out.FindAuthorByIdPort;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistAuthorPort;
import hu.me.krz.haladojava.hexagon.domain.Author;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class AuthorPersistenceAdapter implements PersistAuthorPort, FindAuthorByIdPort {
	private final AuthorRepository authorRepository;

	@Override
	public Author saveAuthor(Author author) {
		return authorRepository.save(author);
	}

	@Override
	public Optional<Author> findAuthorById(Long authorId) throws AuthorNotFoundException {
		return authorRepository.findById(authorId);
	}
}
