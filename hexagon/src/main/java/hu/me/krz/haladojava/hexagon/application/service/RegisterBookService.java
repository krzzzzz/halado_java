package hu.me.krz.haladojava.hexagon.application.service;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterBookUseCase;
import hu.me.krz.haladojava.hexagon.application.port.out.FindAuthorByIdPort;
import hu.me.krz.haladojava.hexagon.application.port.out.FindBookByTitlePort;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistBookPort;
import hu.me.krz.haladojava.hexagon.common.NoSuchAuthorException;
import hu.me.krz.haladojava.hexagon.common.UseCase;
import hu.me.krz.haladojava.hexagon.domain.Author;
import hu.me.krz.haladojava.hexagon.domain.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@UseCase
@RequiredArgsConstructor
@Slf4j
public class RegisterBookService implements RegisterBookUseCase {

	private final FindAuthorByIdPort findAuthorByIdPort;
	private final FindBookByTitlePort findBookByTitlePort;
	private final PersistBookPort persistBookPort;

	@Override
	public void registerBook(RegisterBookCommand command) {
		Author author = findAuthorByIdPort.findAuthorById(
				command.getAuthorId()).orElseThrow(() -> new NoSuchAuthorException(command.getAuthorId()));

		requireUniqueTitle(command.getBookTitle());

		Book book = new Book(null, command.getBookTitle(), author.getId());
		persistBookPort.saveBook(book);
		log.debug(book.toString());
	}

	private void requireUniqueTitle(String bookTitle) {
		if (findBookByTitlePort.findBookByTitle(bookTitle).isPresent()) {
			throw new NonUniqueBookTitleException(bookTitle);
		}
	}


}
