package hu.me.krz.haladojava.hexagon.adapter.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterBookUseCase;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RegisterBookController {
	private final RegisterBookUseCase bookUseCase;

	@PostMapping("books/register")
	void registerBook(@RequestBody RegisterBookResource registerBookResource) {
		bookUseCase.registerBook(registerBookResource.toCommand());
	}
}
