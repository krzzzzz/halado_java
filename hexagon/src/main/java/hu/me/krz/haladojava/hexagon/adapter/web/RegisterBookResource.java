package hu.me.krz.haladojava.hexagon.adapter.web;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterBookUseCase;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class RegisterBookResource {
	@NotEmpty
	private String bookTitle;

	@NotNull
	private Long authorId;

	public RegisterBookUseCase.RegisterBookCommand toCommand() {
		return new RegisterBookUseCase.RegisterBookCommand(bookTitle, authorId);
	}
}
