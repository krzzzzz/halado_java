package hu.me.krz.haladojava.hexagon.application.port.out;

import java.util.Optional;

import hu.me.krz.haladojava.hexagon.domain.Author;

public interface FindAuthorByIdPort {
	Optional<Author> findAuthorById(Long authorId) throws AuthorNotFoundException;

	class AuthorNotFoundException extends RuntimeException {
		public AuthorNotFoundException(Long authorId) {
			super(String.format("Author with ID %d does not exist!", authorId));
		}
	}
}
