package hu.me.krz.haladojava.hexagon.application.port.out;

import java.util.Optional;

import hu.me.krz.haladojava.hexagon.domain.Book;

public interface FindBookByTitlePort {
	Optional<Book> findBookByTitle(String bookTitle);
}
