package hu.me.krz.haladojava.hexagon.application.port.out;

import hu.me.krz.haladojava.hexagon.domain.Author;

public interface PersistAuthorPort {
	Author saveAuthor(Author author);
}
