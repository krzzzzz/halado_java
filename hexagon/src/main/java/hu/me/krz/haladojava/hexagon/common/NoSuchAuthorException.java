package hu.me.krz.haladojava.hexagon.common;

public class NoSuchAuthorException extends RuntimeException {
	public NoSuchAuthorException(Long authotId) {
		super(String.format("No such Author %d", authotId));
	}
}
