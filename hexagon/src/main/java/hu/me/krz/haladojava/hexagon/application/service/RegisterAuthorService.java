package hu.me.krz.haladojava.hexagon.application.service;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistAuthorPort;
import hu.me.krz.haladojava.hexagon.common.UseCase;
import hu.me.krz.haladojava.hexagon.domain.Author;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@UseCase
@RequiredArgsConstructor
@Slf4j
public class RegisterAuthorService implements RegisterAuthorUseCase {
	private final PersistAuthorPort persistAuthorPort;
	@Override
	public void registerAuthor(RegisterAuthorCommand command) {
		Author author = persistAuthorPort.saveAuthor(new Author(null, command.getName()));
		log.debug(author.toString());
	}
}
