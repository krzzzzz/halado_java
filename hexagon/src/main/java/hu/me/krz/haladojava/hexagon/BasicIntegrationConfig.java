package hu.me.krz.haladojava.hexagon;

import java.io.File;
import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableIntegration
public class BasicIntegrationConfig{
	public String INPUT_DIR = "authors";
	public String FILE_PATTERN = "*.json";

	@Bean
	public MessageChannel authorFileNameChannel() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel authorInstanceChannel() {
		return new DirectChannel();
	}

	@Bean
	@InboundChannelAdapter(value = "authorFileNameChannel", poller = @Poller(fixedDelay = "1000"))
	public MessageSource<File> fileReadingMessageSource() {
		FileReadingMessageSource sourceReader= new FileReadingMessageSource();
		sourceReader.setDirectory(new File(INPUT_DIR));
		sourceReader.setFilter(
				new CompositeFileListFilter<>(
						Arrays.asList(
								new SimplePatternFileListFilter(FILE_PATTERN),
								new AcceptOnceFileListFilter<>(10)

						)));
		return sourceReader;
	}
}
