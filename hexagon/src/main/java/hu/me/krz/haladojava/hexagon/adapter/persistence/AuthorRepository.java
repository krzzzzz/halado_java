package hu.me.krz.haladojava.hexagon.adapter.persistence;

import org.springframework.data.repository.CrudRepository;

import hu.me.krz.haladojava.hexagon.domain.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
