package hu.me.krz.haladojava.hexagon.adapter.integration.transformer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import hu.me.krz.haladojava.hexagon.adapter.web.RegisterAuthorResource;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.Message;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@MessageEndpoint
@Slf4j
public class FileNameToAuthorTransformer {
	@Transformer(inputChannel = "authorFileNameChannel", outputChannel = "authorInstanceChannel")
	RegisterAuthorUseCase.RegisterAuthorCommand transform(Message<?> msg) throws IOException {
		File file = (File) msg.getPayload();
		log.debug("incoming filename: {}", file.toString());
		byte[] jsonData = Files.readAllBytes(file.toPath());
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.readValue(jsonData, RegisterAuthorResource.class).toCommand();
	}
}
