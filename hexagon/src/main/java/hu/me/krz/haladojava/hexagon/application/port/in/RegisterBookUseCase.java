package hu.me.krz.haladojava.hexagon.application.port.in;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import org.hibernate.validator.constraints.NotBlank;

public interface RegisterBookUseCase  {
	void registerBook(RegisterBookCommand command);

	@Getter
	final class RegisterBookCommand extends SelfValidating<RegisterBookCommand>{
		@NotBlank
		private final String bookTitle;

		@NotNull
		private final Long authorId;

		public RegisterBookCommand(@NotBlank String bookTitle, @NotNull Long authorId) {
			this.bookTitle = bookTitle;
			this.authorId = authorId;
			validateSelf();
		}
	}

	final class NonUniqueBookTitleException extends RuntimeException {

		public NonUniqueBookTitleException(String bookTitle) {
			super(String.format("Book exist with this title %s", bookTitle));
		}
	}
}
