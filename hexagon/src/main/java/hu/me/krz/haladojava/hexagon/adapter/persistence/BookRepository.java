package hu.me.krz.haladojava.hexagon.adapter.persistence;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hu.me.krz.haladojava.hexagon.domain.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
	Optional<Book> findByTitle(String bookTitle);
}
