package hu.me.krz.haladojava.hexagon.application.port.in;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;

public interface RegisterAuthorUseCase {

	void registerAuthor(RegisterAuthorCommand command);

	@Getter
	final class RegisterAuthorCommand extends SelfValidating<RegisterAuthorCommand>{
		@NotNull
		private final String name;

		public RegisterAuthorCommand(@NotEmpty String name) {
			this.name = name;
			validateSelf();
		}
	}
}
