package hu.me.krz.haladojava.hexagon.adapter.web;

import javax.validation.constraints.NotEmpty;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class RegisterAuthorResource {
	@NotEmpty
	private String name;

	public RegisterAuthorUseCase.RegisterAuthorCommand toCommand() {
		return new RegisterAuthorUseCase.RegisterAuthorCommand(name);
	}
}
