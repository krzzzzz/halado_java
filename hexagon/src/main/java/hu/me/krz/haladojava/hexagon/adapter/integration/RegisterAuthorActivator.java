package hu.me.krz.haladojava.hexagon.adapter.integration;

import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@MessageEndpoint
@Slf4j
@RequiredArgsConstructor
public class RegisterAuthorActivator {
	private final RegisterAuthorUseCase registerAuthorUseCase;

	@ServiceActivator(inputChannel= "authorInstanceChannel")
	public void registerBook(Message<?> msg) {
		RegisterAuthorUseCase.RegisterAuthorCommand registerAuthorCommand = (RegisterAuthorUseCase.RegisterAuthorCommand) msg.getPayload();
		log.debug( "{} is coming", registerAuthorCommand);

		registerAuthorUseCase.registerAuthor(registerAuthorCommand);
	}
}
