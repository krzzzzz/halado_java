package hu.me.krz.haladojava.hexagon.adapter.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RegisterAuthorController {
	private final RegisterAuthorUseCase registerAuthorUseCase;

	@PostMapping("authors/register")
	void registerAuthor(@RequestBody RegisterAuthorResource registerAuthorResource) {
		registerAuthorUseCase.registerAuthor(registerAuthorResource.toCommand());
	}
}
