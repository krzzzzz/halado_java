package hu.me.krz.haladojava.hexagon.adapter.persistence;

import java.util.Optional;

import org.springframework.stereotype.Component;

import hu.me.krz.haladojava.hexagon.application.port.out.FindBookByTitlePort;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistBookPort;
import hu.me.krz.haladojava.hexagon.domain.Book;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class BookPersistenceAdapter implements FindBookByTitlePort, PersistBookPort {
	private final BookRepository bookRepository;

	@Override
	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public Optional<Book> findBookByTitle(String bookTitle) {
		return bookRepository.findByTitle(bookTitle);
	}
}
