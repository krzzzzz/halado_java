package hu.me.krz.haladojava.hexagon.adapter.web;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import hu.me.krz.haladojava.hexagon.application.port.in.RegisterBookUseCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class RegisterBookControllerTest {

    @Captor
    ArgumentCaptor<RegisterBookUseCase.RegisterBookCommand> registerBookCommandArgumentCaptor;

    @Mock
    RegisterBookUseCase registerBookUseCase;

    @InjectMocks
    RegisterBookController registerBookController;

    @Test
    void registerBook() {
        // GIVEN
        final String name = "Demo book";
        final Long authorId = 1L;
        RegisterBookResource registerBookResource = new RegisterBookResource();
        registerBookResource.setBookTitle(name);
        registerBookResource.setAuthorId(authorId);

        // WHEN
        registerBookController.registerBook(registerBookResource);

        // THEN
        verify(registerBookUseCase).registerBook(registerBookCommandArgumentCaptor.capture());
        assertEquals(name, registerBookCommandArgumentCaptor.getValue().getBookTitle());
        assertEquals(authorId, registerBookCommandArgumentCaptor.getValue().getAuthorId());
    }
}