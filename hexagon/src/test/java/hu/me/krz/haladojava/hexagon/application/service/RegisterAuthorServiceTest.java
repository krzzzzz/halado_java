package hu.me.krz.haladojava.hexagon.application.service;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistAuthorPort;
import hu.me.krz.haladojava.hexagon.domain.Author;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RegisterAuthorServiceTest {

    @Captor
    ArgumentCaptor<Author> authorCaptor;

    @Mock
    PersistAuthorPort persistAuthorPort;

    @InjectMocks
    RegisterAuthorService registerAuthorService;

    @Test
    void registerAuthor() {
        // GIVEN
        final String name = "DEMO";
        RegisterAuthorUseCase.RegisterAuthorCommand registerAuthorCommand =
                new RegisterAuthorUseCase.RegisterAuthorCommand(name);

        when(persistAuthorPort.saveAuthor(any(Author.class))).thenAnswer(i -> i.getArguments()[0]);

        // WHEN
        registerAuthorService.registerAuthor(registerAuthorCommand);

        // THEN
        verify(persistAuthorPort).saveAuthor(authorCaptor.capture());
        assertEquals(name, authorCaptor.getValue().getName());
    }
}