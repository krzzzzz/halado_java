package hu.me.krz.haladojava.hexagon.adapter.persistence;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import hu.me.krz.haladojava.hexagon.application.port.out.FindAuthorByIdPort;
import hu.me.krz.haladojava.hexagon.domain.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookPersistenceAdapterTest {

    @Mock
    BookRepository bookRepository;

    @InjectMocks
    BookPersistenceAdapter bookPersistenceAdapter;

    @Test
    void saveBook() {
        // GIVEN
        final Book book = new Book(1L, "title", 1L);

        // WHEN
        bookPersistenceAdapter.saveBook(book);

        // THEN
        verify(bookRepository).save(book);
    }

    @Test
    void findBookByTitle() {
        // GIVEN
        final String title = "TITLE";
        final Book book = new Book(1L, "title", 1L);
        when(bookRepository.findByTitle(title)).thenReturn(Optional.of(book));

        // WHEN
        Optional<Book> bookByTitle = bookPersistenceAdapter.findBookByTitle(title);

        // THEN
        assertEquals(book, bookByTitle.get());
    }

    @Test
    void findBookByTitle_exception() {
        // GIVEN
        final String title = "TITLE";
        final Long authodId = 1L;
        when(bookRepository.findByTitle(title)).thenThrow(new FindAuthorByIdPort.AuthorNotFoundException(authodId));

        // WHEN // THEN
        try {
            bookPersistenceAdapter.findBookByTitle(title);
            fail();
        } catch (FindAuthorByIdPort.AuthorNotFoundException ex) {
            assertTrue(ex.getMessage().contains(authodId.toString()));
        }
    }
}