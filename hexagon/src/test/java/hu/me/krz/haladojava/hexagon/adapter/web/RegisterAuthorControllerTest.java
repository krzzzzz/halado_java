package hu.me.krz.haladojava.hexagon.adapter.web;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterAuthorUseCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class RegisterAuthorControllerTest {

    @Captor
    ArgumentCaptor<RegisterAuthorUseCase.RegisterAuthorCommand> registerAuthorCommandCaptor;

    @Mock
    RegisterAuthorUseCase registerAuthorUseCase;

    @InjectMocks
    RegisterAuthorController registerAuthorController;

    @Test
    void registerAuthor() {
        // GIVEN
        final String name = "Demo";
        RegisterAuthorResource registerAuthorResource = new RegisterAuthorResource();
        registerAuthorResource.setName(name);

        // WHEN
        registerAuthorController.registerAuthor(registerAuthorResource);

        // THEN
        verify(registerAuthorUseCase).registerAuthor(registerAuthorCommandCaptor.capture());
        assertEquals(name, registerAuthorCommandCaptor.getValue().getName());
    }
}