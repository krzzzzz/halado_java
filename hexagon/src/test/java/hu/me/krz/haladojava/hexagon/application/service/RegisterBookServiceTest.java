package hu.me.krz.haladojava.hexagon.application.service;

import hu.me.krz.haladojava.hexagon.application.port.in.RegisterBookUseCase;
import hu.me.krz.haladojava.hexagon.application.port.out.FindAuthorByIdPort;
import hu.me.krz.haladojava.hexagon.application.port.out.FindBookByTitlePort;
import hu.me.krz.haladojava.hexagon.application.port.out.PersistBookPort;
import hu.me.krz.haladojava.hexagon.common.NoSuchAuthorException;
import hu.me.krz.haladojava.hexagon.domain.Author;
import hu.me.krz.haladojava.hexagon.domain.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RegisterBookServiceTest {

    @Mock
    FindAuthorByIdPort findAuthorByIdPort;

    @Mock
    FindBookByTitlePort findBookByTitlePort;

    @Mock
    PersistBookPort persistBookPort;

    @InjectMocks
    RegisterBookService registerBookService;

    @Captor
    ArgumentCaptor<Book> bookArgumentCaptor;

    @Test
    void registerBook() {
        // GIVEN
        final Long authorId = 1L;
        final String bookTitle = "TITLE";
        final Author author = new Author(6L, "authorName");

        RegisterBookUseCase.RegisterBookCommand registerBookCommand =
                new RegisterBookUseCase.RegisterBookCommand(bookTitle, authorId);
        when(findBookByTitlePort.findBookByTitle(bookTitle)).thenReturn(Optional.empty());
        when(findAuthorByIdPort.findAuthorById(authorId)).thenReturn(Optional.of(author));

        // WHEN
        registerBookService.registerBook(registerBookCommand);

        // THEN
        verify(persistBookPort).saveBook(bookArgumentCaptor.capture());
    }

    @Test
    void registerBook_NonUniqueBookTitle() {
        // GIVEN
        final Long authorId = 1L;
        final String bookTitle = "TITLE";
        final Author author = new Author(6L, "authorName");
        final Book book = new Book(3L, bookTitle, authorId);

        RegisterBookUseCase.RegisterBookCommand registerBookCommand =
                new RegisterBookUseCase.RegisterBookCommand(bookTitle, authorId);
        when(findBookByTitlePort.findBookByTitle(bookTitle)).thenReturn(Optional.of(book));
        when(findAuthorByIdPort.findAuthorById(authorId)).thenReturn(Optional.of(author));

        // WHEN
        try {
            registerBookService.registerBook(registerBookCommand);
            fail();
        } catch (RegisterBookUseCase.NonUniqueBookTitleException ex) {
            assertTrue(ex.getMessage().contains(bookTitle));
        }

        // THEN
        verify(persistBookPort, never()).saveBook(bookArgumentCaptor.capture());
    }

    @Test
    void registerBook_NoSuchAuthor() {
        // GIVEN
        final Long authorId = 1L;
        final String bookTitle = "TITLE";


        RegisterBookUseCase.RegisterBookCommand registerBookCommand =
                new RegisterBookUseCase.RegisterBookCommand(bookTitle, authorId);
        when(findAuthorByIdPort.findAuthorById(authorId)).thenThrow(new NoSuchAuthorException(authorId));

        // WHEN
        try {
            registerBookService.registerBook(registerBookCommand);
            fail();
        } catch (NoSuchAuthorException ex) {
            assertTrue(ex.getMessage().contains(authorId.toString()));
        }

        // THEN
        verify(persistBookPort, never()).saveBook(bookArgumentCaptor.capture());
    }
}