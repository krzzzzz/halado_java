import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

public class Csak {

    private int a;
    private String hello;

    public static void main(String[] args) {

        List<String> fruits = new ArrayList<String>();
        fruits.add("apple");
        fruits.add("banana");
        fruits.add("peach");
        fruits.add("apple");
        fruits.add("banana");
        fruits.add("apple");
        fruits.add("cherry");
        System.out.println("fruits[2] = " + fruits.get(2));
        fruits.set(1, "grape");

        for (String fruit : fruits) {
            System.out.println(fruit);
        }
        Collections.sort(fruits);
        System.out.println("Sorted:");
        for (String fruit : fruits) {
            System.out.println(fruit);
        }

        Map<String, Integer> nameMap = new HashMap<>();

        Csak csak = new Csak();

        Predicate<String> notEmpty = s -> ! s.trim().isEmpty();
        Predicate<String> notContainsY = s -> s.indexOf('Y') == -1;



        List<String> lista = new ArrayList<>();

        Predicate<Integer> isGreaterThan0 = number -> number > 0;

        Function<String, Integer> string2int = item -> item.length();

        List<Integer> lengthList =
                lista.stream()
                .map( string2int )
                .filter( isGreaterThan0 )
                        .collect(Collectors.toList());


        List<Integer> lList = new ArrayList<>();
        for (String item: lista) {
            int length = item.length();
            if (length > 0) {
                lList.add(length);
            }
        }

        List<User> userList = new ArrayList<>();

        userList.stream().map( user -> new UserDto(user)).collect(Collectors.toList());




        csak.kiirHaOk( notEmpty.and(notContainsY), "sadfasdfYasdf");
    }

    public static void dd(Double d) {
        System.out.println(d);
    }
    public static int length(String s) {
        return s.length();
    }

    public static int ize(int a) {
        return (int) Math.random() * a ;
    }

    private int v(String s) {
        return s.indexOf('d');
    }

    private void printResult(Function<Integer, Integer> fv) {
        int result = fv.apply(5);
        System.out.println(result);
    }


    private void printResult(DoubleSupplier fv) {
        Double result = fv.getAsDouble();
        System.out.println(result);
    }

    private void dataIn(Consumer<Double> fv) {
        fv.accept(8.0);
    }


    private void kiirHaOk(Predicate<String> pred, String s) {
        if (pred.test(s)) {
            System.out.println("-" + s + "-");
        }
    }

    public static class User {
        private String name;
        private int age;
        private int salary;

        public User(String name, int age, int salary) {
            this.name = name;
            this.age = age;
            this.salary = salary;
        }


        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public int getSalary() {
            return salary;
        }
    }

    public static class UserDto {
        private String name;
        private int age;

        public UserDto(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public UserDto(User user) {
            this.name = user.getName();
            this.age = user.getAge();
        }
    }
}
